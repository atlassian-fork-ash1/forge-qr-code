# Forge QR Code Macro 

This Forge app adds a QR Code macro to your Confluence Cloud site.

![QR Code Config](images/qr-code-config.png)

![QR Code](images/qr-code.png)

## Architecture

The app's [manifest.yml](./manifest.yml) contains two modules:

1. a [macro module](https://developer.atlassian.com/platform/forge/manifest-reference/#macro) that specifies the metadata displayed to the user in the Confluence editor's "quick insert" menu.
2. a corresponding [function module](https://developer.atlassian.com/platform/forge/manifest-reference/#function) that implements the macro logic. 

The function logic is implemented wholly in a single file: [src/index.tsx](./src/index.tsx). 

The app has two logical states, `Configure` and `Display`:

```
[Configuration] -> [Display]
```

Note: When a QR Code macro is first inserted into some content, it starts in the `Configure` state. Once configured, the macro enters the `Display` state, and there is no way to further adjust the configuration of the macro. Instead, the user is expected to simply delete and then reinsert the macro. This is a temporary workaround until macro configuration is implemented in Forge.

The macro solicits configuration from the user using a Forge UI [Form component](https://developer.atlassian.com/platform/forge/ui-components/form/). When the user submits the form, the data is stored (and later retrieved) using the [useContentProperty](https://developer.atlassian.com/platform/forge/ui-hooks-referenceH/#usecontentproperty) UI hook. 

Once configured, the function renders the configured values as a QR code using [qrcode-svg](https://www.npmjs.com/package/qrcode-svg), and then encodes it as a data URI using [datauri](https://www.npmjs.com/package/datauri). The QR Code is then displayed to the user by returning the data URI as the `src` attribute of a Forge UI [Image component](https://developer.atlassian.com/platform/forge/ui-components/image/).

## Installing this app
 
You will need Node.js and the Forge CLI to install this app. You can install the Forge CLI by running `npm install -g @forge/cli`.

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository.
1. Run `forge register` to register a new copy of this app to your developer account.
1. Run `npm install` to install your dependencies.
1. Run `forge deploy` to deploy the app into the default environment.
1. Run `forge install` and follow the prompts to install the app into your Confluence site.

The **QR Code** macro should now appear in your macro browser.

At this point, you now have your own copy of a Forge app. 
