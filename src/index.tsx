import ForgeUI, { render, useConfig, Fragment, Image, TextField, Select, Option, ConfigForm, Macro } from "@forge/ui";

import QRCode from "qrcode-svg";

const defaultConfig = {
  url: "https://www.atlassian.com",
  color: "172B4D"
};

const Config = () => {
  return (
    <ConfigForm>
      <TextField isRequired={true} label="URL" name="url" />
      <Select label="Color" name="color">
        <Option label="N800 - Squid ink" value="172B4D" />
        <Option label="B400 - Pacific bridge" value="0052CC" />
        <Option label="R500 - Dragon's blood" value="BF2600" />
        <Option label="Y400 - Cheezy blasters" value="FF991F" />
        <Option label="G400 - Slime" value="00875A" />
        <Option label="T400 - Prom dress" value="00A3BF" />
        <Option label="P300 - Da' juice" value="6554C0" />
      </Select>
    </ConfigForm>
  );
}

const App = () => {
  const { url, color } = useConfig();
  const qrCode = new QRCode({
    content: url,
    padding: 4,
    width: 256,
    height: 256,
    color: "#" + color,
    background: "#ffffff",
    ecl: "M",
  });

  const encodedSvg = new Buffer(qrCode.svg()).toString('base64');
  const dataUri = `data:image/svg+xml;base64,${encodedSvg}`;

  return (
    <Fragment>
      <Image src={dataUri} alt={url} />
    </Fragment>
  );
};

export const run = render(
  <Macro 
    app={<App />}
    config={<Config />}
    defaultConfig={defaultConfig}
  />
);
